package org.usfirst.frc.team3986.robot;



import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Timer;


public class Robot extends IterativeRobot {

	CANTalonPublisher canTalonPublisher;
	CANTalon canTalon;
	
	@Override
	public void robotInit() {
		
		canTalon = new CANTalon(1);
		canTalon.configNominalOutputVoltage(0.0, 0.0);
		canTalon.configPeakOutputVoltage(0.0, -12.0);
		canTalon.changeControlMode(TalonControlMode.Speed);
		canTalon.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);
		canTalon.reverseSensor(true);
		canTalon.setPID(0.1, 0, 1.0, 0.0268, 0, 0, 0);
//		canTalon.setPID(0.08, 0, 0.8, 0.0323, 0, 0, 0);
		canTalonPublisher = new CANTalonPublisher(canTalon, true, true);
		super.robotInit();
	}
	
	public void teleopPeriodic() {
	

		canTalon.set(-3400);
		
		
		canTalonPublisher.publishStatus();
		
		Timer.delay(0.5);
	}
}
