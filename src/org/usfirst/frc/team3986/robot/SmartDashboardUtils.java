package org.usfirst.frc.team3986.robot;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SmartDashboardUtils {

	private static final String SEPARATOR = ";";
	private static final String EQUALS = ":";
	private SmartDashboardUtils(){}

	public static StringBuilder publishAndAppend(String key, double value, int numDecimals,
			final StringBuilder sb){
		SmartDashboard.putNumber(key, value);
		sb.append(key)
		.append(EQUALS)
		.append(String.format("%f." + numDecimals, value))
		.append(SEPARATOR);
		return sb;
	}

	public static StringBuilder publishAndAppend(String key, String str,final StringBuilder sb){
		SmartDashboard.putString(key, str);
		sb.append(key)
		.append(EQUALS)
		.append(str)
		.append(SEPARATOR);
		return sb;
	}
	
	public static StringBuilder publishAndAppend(String key, Long num, final StringBuilder sb){
		SmartDashboard.putNumber(key, num);
		sb.append(key)
		.append(EQUALS)
		.append(num)
		.append(SEPARATOR);
		return sb;
	}
	public static StringBuilder publishAndAppend(String key, Boolean bool, final StringBuilder sb){
		SmartDashboard.putBoolean(key, bool);
		sb.append(key)
		.append(EQUALS)
		.append(bool)
		.append(SEPARATOR);
		return sb;
	}

}
