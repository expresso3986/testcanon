package org.usfirst.frc.team3986.robot;

public interface IDistanceProvider {
	
	double getDistance();
	boolean isInRange();

}
