package org.usfirst.frc.team3986.robot;

import edu.wpi.first.wpilibj.AnalogInput;

public class AnalogSonar implements IDistanceProvider{
	
	private final AnalogInput analogInput;
	private final double distancePerUnit;
	private final double intercept;
	private final double minUnitRange;
	private final double maxUnitRange;
	
	public AnalogSonar(AnalogInput analogInput, double unitsPerVolt, double intercept, double minUnitRange, double maxUnitRange) {
		this.analogInput = analogInput;
		this.distancePerUnit = unitsPerVolt;
		this.intercept = intercept;
		this.minUnitRange = minUnitRange;
		this.maxUnitRange = maxUnitRange;
	}

	public double getAverageValue(){
		return analogInput.getAverageValue();
	}
	
	@Override
	public double getDistance(){
		return getAverageValue() * distancePerUnit + intercept;
	}

	@Override
	public boolean isInRange() {
		double averageValue = getAverageValue();
		return averageValue > minUnitRange && averageValue < maxUnitRange;
	}

}
