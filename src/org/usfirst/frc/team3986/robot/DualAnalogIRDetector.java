package org.usfirst.frc.team3986.robot;

public class DualAnalogIRDetector implements IPublishStatus, IDistanceProvider, IAngularProvider{
	
	private final AnalogIRDetector leftDetector;
	private final AnalogIRDetector rightDetector;
	private final double separationDistance;

	public DualAnalogIRDetector(AnalogIRDetector leftSonar, AnalogIRDetector rightSonar, double separationDistance) {
		this.leftDetector = leftSonar;
		this.rightDetector = rightSonar;
		this.separationDistance = separationDistance;
		if(separationDistance <= 0)
			throw new IllegalArgumentException("separationDistance must be positive");
	}

	@Override
	public double getAngle(){	
		double diff = leftDetector.getDistance() - rightDetector.getDistance();
		return calcAngle(diff, separationDistance);
	}

	private static double calcAngle(double x, double y) {
		return Math.atan(x/y) * 180 / Math.PI;
	}
	
	@Override
	public double getDistance(){
		return ( leftDetector.getDistance() + rightDetector.getDistance())/2;
	}

	@Override
	public boolean isReady(){
		return isInRange();
	}
	
	@Override
	public boolean isInRange() {
		return leftDetector.isInRange() && rightDetector.isInRange();
	}
	
	public static void main(String[] args){
		//for testing
		System.out.println(calcAngle(5, 10));
		System.out.println(calcAngle(-5, 10));
		System.out.println(calcAngle(10, 10));
		System.out.println(calcAngle(-10, 10));
	}

	@Override
	public void publishStatus() {
		StringBuilder sb = new StringBuilder();
		SmartDashboardUtils.publishAndAppend("dualAnalogIRDetector isInRange", isInRange(), sb);
		SmartDashboardUtils.publishAndAppend("dualAnalogIRDetector distance", getDistance(),0, sb);
		SmartDashboardUtils.publishAndAppend("dualAnalogIRDetector angle", getAngle(), 1, sb);
		SmartDashboardUtils.publishAndAppend("leftIRDetector distance", leftDetector.getDistance(), 0, sb);
		SmartDashboardUtils.publishAndAppend("leftIRDetector average voltage", leftDetector.getAverageVoltage(), 2, sb);
		SmartDashboardUtils.publishAndAppend("rightIRDetector distance", rightDetector.getDistance(), 0, sb);
		SmartDashboardUtils.publishAndAppend("rightIRDetector average voltage", rightDetector.getAverageVoltage(), 2, sb);
		System.out.println(sb);
	}

	public IDistanceProvider getLeftSonar() {
		return leftDetector;
	}
	
	public IDistanceProvider getRightSonar() {
		return rightDetector;
	}
}
