package org.usfirst.frc.team3986.robot;

import com.ctre.CANTalon;

public class CANTalonPublisher implements IPublishStatus {

	private final CANTalon canTalon;
	private final boolean isDetailed;
	private final boolean isSpeed;

	public CANTalonPublisher(CANTalon canTalon, boolean isDetailed, boolean isSpeed){
		this.canTalon = canTalon;
		this.isDetailed = isDetailed;
		this.isSpeed = isSpeed;
	}

	@Override
	public void publishStatus() {
		StringBuilder sb = new StringBuilder();
		if(isDetailed){
			SmartDashboardUtils.publishAndAppend("CAN Talon ControlMode", canTalon.getControlMode().toString(), sb );
			SmartDashboardUtils.publishAndAppend("CAN Talon PIDSourceType", canTalon.getPIDSourceType().toString(), sb );
		}
		SmartDashboardUtils.publishAndAppend("CAN Talon get", canTalon.get(), 1, sb );
		if(isDetailed  || !isSpeed)
			SmartDashboardUtils.publishAndAppend("CAN Talon position", canTalon.getPosition(), 1, sb );
		SmartDashboardUtils.publishAndAppend("CAN Talon speed", canTalon.getSpeed(), 1, sb );
		SmartDashboardUtils.publishAndAppend("CAN Talon setPoint", canTalon.getSetpoint(), 1, sb );
		if(isDetailed){
			SmartDashboardUtils.publishAndAppend("CAN Talon error", canTalon.getError(), 1, sb );
			SmartDashboardUtils.publishAndAppend("CAN Talon encPosition", canTalon.getEncPosition(), 1, sb );
			SmartDashboardUtils.publishAndAppend("CAN Talon EncVelocity", canTalon.getEncVelocity(), 1, sb );
			SmartDashboardUtils.publishAndAppend("CAN Talon ClosedLoopError", canTalon.getClosedLoopError(),1, sb );
		}
		System.out.println(sb);


	}

}
