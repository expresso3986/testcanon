package org.usfirst.frc.team3986.robot;

public class DualAnalogSonar implements IPublishStatus, IDistanceProvider, IAngularProvider{
	
	private final AnalogSonar leftSonar;
	private final AnalogSonar rightSonar;
	private final double separationDistance;

	public DualAnalogSonar(AnalogSonar leftSonar, AnalogSonar rightSonar, double separationDistance) {
		this.leftSonar = leftSonar;
		this.rightSonar = rightSonar;
		this.separationDistance = separationDistance;
		if(separationDistance <= 0)
			throw new IllegalArgumentException("separationDistance must be positive");
	}

	@Override
	public double getAngle(){	
		double diff = leftSonar.getDistance() - rightSonar.getDistance();
		return calcAngle(diff, separationDistance);
	}

	private static double calcAngle(double x, double y) {
		return Math.atan(x/y) * 180 / Math.PI;
	}
	
	@Override
	public double getDistance(){
		return ( leftSonar.getDistance() + rightSonar.getDistance())/2;
	}

	@Override
	public boolean isReady(){
		return isInRange();
	}
	
	@Override
	public boolean isInRange() {
		return leftSonar.isInRange() && rightSonar.isInRange();
	}
	
	public static void main(String[] args){
		//for testing
		System.out.println(calcAngle(5, 10));
		System.out.println(calcAngle(-5, 10));
		System.out.println(calcAngle(10, 10));
		System.out.println(calcAngle(-10, 10));
	}

	@Override
	public void publishStatus() {
		StringBuilder sb = new StringBuilder();
		SmartDashboardUtils.publishAndAppend("dualAnalogSonar isInRange", isInRange(), sb);
		SmartDashboardUtils.publishAndAppend("dualAnalogSonar distance", getDistance(),0, sb);
		SmartDashboardUtils.publishAndAppend("dualAnalogSonar angle", getAngle(), 1, sb);
		SmartDashboardUtils.publishAndAppend("leftSonar distance", leftSonar.getDistance(), 0, sb);
		SmartDashboardUtils.publishAndAppend("leftSonar average value", leftSonar.getAverageValue(), 2, sb);
		SmartDashboardUtils.publishAndAppend("rightSonar distance", rightSonar.getDistance(), 0, sb);
		SmartDashboardUtils.publishAndAppend("rightSonar average value", rightSonar.getAverageValue(), 2, sb);
		System.out.println(sb);
	}

	public IDistanceProvider getLeftSonar() {
		return leftSonar;
	}
	
	public IDistanceProvider getRightSonar() {
		return rightSonar;
	}
}
