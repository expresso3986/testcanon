package org.usfirst.frc.team3986.robot;

import java.util.Arrays;

import edu.wpi.first.wpilibj.AnalogInput;

public class AnalogIRDetector implements IDistanceProvider{
	
	private final AnalogInput analogInput;
	private final double[][] voltageDistanceArray;
	private final int lastIndex ;
	
	
	public AnalogIRDetector(AnalogInput analogInput) {
		this(analogInput, new double[][]{{0.35, 75},{0.45, 61}, {0.54, 53}, 
			{0.64, 44},{0.84, 31} ,{1.09, 24} ,{1.36, 18}, {2.2, 10}});
	}
	
	public AnalogIRDetector(AnalogInput analogInput, double[][] voltageDistanceArray) {
		this.analogInput = analogInput;
		this.voltageDistanceArray = voltageDistanceArray; //Arrays.sort(voltageDistanceArray,);
		System.out.println(Arrays.deepToString(voltageDistanceArray));
		
		lastIndex = voltageDistanceArray.length -1;
	}

	public double getAverageVoltage(){
		return analogInput.getAverageVoltage();
	}
	
	@Override
	public double getDistance(){
		double averageVoltage = getAverageVoltage();
		return calcDistance(averageVoltage);
	}

	private double  calcDistance(double averageVoltage) {
		int i = findPointBefore(averageVoltage);
		if(i == -1)
			return voltageDistanceArray[0][1];
		if(i == lastIndex)
			return voltageDistanceArray[lastIndex][1];
		
		return interpolate(i, averageVoltage);
	}

	private  double interpolate(int index, double averageVoltage) {
		double v0 = voltageDistanceArray[index][0];
		double v1 = voltageDistanceArray[index+1][0];
		double d0 = voltageDistanceArray[index][1];
		double d1 = voltageDistanceArray[index+1][1];
		return d0 + (d1-d0)/(v1-v0) * (averageVoltage - v0);
	}

	private int findPointBefore(double averageVoltage) {
		int i = 0;
			while(i <= lastIndex && averageVoltage > voltageDistanceArray[i][0]){
				i++;
			}
		return i-1;
	}

	@Override
	public boolean isInRange() {
		double averageVoltage = getAverageVoltage();
		return isInRange(averageVoltage);
	}

	private boolean isInRange(double averageVoltage) {
		int i = findPointBefore(averageVoltage);
		return !(i == -1 || i == lastIndex);
	}
	
	public static void main(String[] args){
		
		AnalogIRDetector ir = new AnalogIRDetector(null);
		test(ir, 0);
		test(ir, 1);
		test(ir, 1.36);
		test(ir, 2.3);
		
	}

	private static void test(AnalogIRDetector ir, double voltage) {
		System.out.println(String.format("volt:%.2f dist:%.2f isInRange:%s findPointBefore:%d", 
				voltage, ir.calcDistance(voltage), ir.isInRange(voltage), ir.findPointBefore(voltage)));
	}

}
