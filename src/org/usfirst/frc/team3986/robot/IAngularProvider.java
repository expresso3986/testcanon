package org.usfirst.frc.team3986.robot;

public interface IAngularProvider {
	
	double getAngle();
	boolean isReady();

}
